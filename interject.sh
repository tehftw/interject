#!/bin/bash

# argument-parsing code taken from: https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash


# A POSIX VARIABLE
OPTIND=1 # reset in case getops has been used previously in the shell

# initialize our variables
text_gnu_slash_linux="GNU/Linux"
text_gnu_plus_linux="GNU+Linux"


text_linux_xfce="Linux+GNU+apt+Xorg+Xfce"
text_end=" "

show_help() {
	echo "'-v' to add vim easteregg"
	echo "'-x' to set GNU+Linux to '$text_linux_xfce'"
	echo "'-p' to set 'GNU/Linux' to 'Linux+GNU'. note, that if you don't also change 'GNU+Linux', then it will look weird :--D"
	echo "'-l <linux>' for custom GNU+Linux"
}


while getopts "h?vxpl:" opt; do
	case "$opt" in
	h|\?)
		show_help
		exit 0
		;;
	v) text_end=";w"
		;;
	x) text_gnu_plus_linux="Linux+GNU+apt+Xorg+Xfce"
		;;
	p) text_gnu_slash_linux="Linux+GNU"
		;;
	l) text_gnu_plus_linux=$OPTARG
		;;
	esac
done






# the most important script
interject_text="I'd just like to interject for moment. What you're refering to as Linux, is in fact, $text_gnu_slash_linux, or as I've recently taken to calling it, $text_gnu_plus_linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX.

Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called Linux, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project.

There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or $text_gnu_slash_linux. All the so-called Linux distributions are really distributions of $text_gnu_slash_linux!"

echo "$interject_text$text_end"
